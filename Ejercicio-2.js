db.getCollection("estudiante").insertMany([
                                            {
                                                "Nombres":"Pablo",
                                                "Apellidos" :"Fernandez Aduviri",
                                                "Edad": 20,
                                                "Sexo":"Masculino",
                                                "fechaNacimiento":new Date(2002,10,25),
                                                "CI": 9362121,
                                                "Incapacidad":"vista",
                                                "nros_telefono": [70738930]
                                            },
                                            {
                                                "Nombres":"Gabriela",
                                                "Apellidos" :"Rojas Loiza",
                                                "Edad": 25,
                                                "Sexo":"Femenino",
                                                "fechaNacimiento":new Date(1998, 7,18),
                                                "CI": 6987462,
                                                "Incapacidad":"ninguna",
                                                "nros_telefono":[1574985]
                                            },
                                            {
                                                "Nombres":"Noel",
                                                "Apellidos" :"Rojas Lino",
                                                "Edad": 20,
                                                "Sexo":"Masculino",
                                                "fechaNacimiento":new Date(2002,5,18),
                                                "CI": 3547852,
                                                "Incapacidad":"ninguna",
                                                "nros_telefono":[6258954,44643224]
                                            },
                                            {
                                                "Nombres":"Fernando",
                                                "Apellidos" :"Rodriguez Ascencio",
                                                "Edad":18,
                                                "Sexo":"Masculino",
                                                "fechaNacimiento":new Date(2004, 11,20),
                                                "CI": 7854925,
                                                "Incapacidad":"ninguna",
                                                "nros_telefono":[677111,4500024]
                                            },
                                            {
                                                "Nombres":"Orlando",
                                                "Apellidos" :"Correa Aguilar",
                                                "Edad":22,
                                                "Sexo":"Masculino",
                                                "fechaNacimiento":new Date(1995,10,6),
                                                "CI": 3658742,
                                                "Incapacidad":"ninguna",
                                                "nros_telefono":[6574213,2654789]
                                            }
                                         ])  


db.getCollection("maestro").insertMany([
                                            {
                                                "Nombres":"Gerardo",
                                                "Apellidos" :"Lopez Ascencio",
                                                "Edad":48,
                                                "Sexo":"Masculino",
                                                "fechaNacimiento":new Date(1969,7,20),
                                                "CI": 9362121,
                                                "DUI": 002548,
                                                "Incapacidad":"ninguna",
                                                "nros_telefono":[2548795]
                                            },
                                            {
                                                "Nombres":"Erlinda",
                                                "Apellidos" :"Martinez Llanos",
                                                "Edad":45,
                                                "Sexo":"Femenino",
                                                "fechaNacimiento":new Date(1970,3,15),
                                                "CI": 3248751,
                                                "DUI": 008476,
                                                "Incapacidad":"vista",
                                                "nros_telefono":[5987467, 4587962]
                                            },
                                            {
                                                "Nombres":"Oscar", 
                                                "Apellidos" :"Morales Becerra",
                                                "Edad":33,
                                                "Sexo":"Masculino",
                                                "fechaNacimiento":new Date(1975,8,3),
                                                "CI": 487596,
                                                "DUI": 005478,
                                                "Incapacidad":"auditiva",
                                                "nros_telefono": [9874265]
                                            },
                                            {
                                                "Nombres":"Lucas",
                                                "Apellidos" :"Taboada Espinoza",
                                                "Edad":35,
                                                "Sexo":"Masculino",
                                                "fechaNacimiento":new Date(1985,10,25),
                                                "CI": 3548752,
                                                "DUI": 006943,
                                                "Incapacidad":"ninguna",
                                                "nros_telefono": [7849246]
                                            },
                                            {
                                                "Nombres":"Dionisio",
                                                "Apellidos" :"Guevara Lopez",
                                                "Edad":36,
                                                "Sexo":"Masculino",
                                                "fechaNacimiento":new Date(1984,9,18),
                                                "CI": 9546325,
                                                "DUI": 003629,
                                                "Incapacidad":"vista",
                                                "nros_telefono":[5976247]
                                            }
                                         ])



db.getCollection("estudiante").createIndex({"Apellidos":1})
db.getCollection("estudiante").createIndex({"nros_telefono":1})
db.getCollection("estudiante").createIndex({"CI":1},{unique:true})


db.getCollection("maestro").createIndex({"Apellidos":1})
db.getCollection("maestro").createIndex({"nros_telefono":1})
db.getCollection("maestro").createIndex({"DUI":1},{unique:true})
db.getCollection("maestro").createIndex({"CI":1},{unique:true})




db.getCollection("estudiante").find({})
db.getCollection("maestro").find({})
